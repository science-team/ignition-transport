ignition-transport (11.0.0+ds-4) unstable; urgency=medium

  * Team upload.

  [ Debian Janitor ]
  * Remove constraints unnecessary since buster

  [ Stephan Lachnit ]
  * zmq.hpp now in cppzmq-dev

  [ Jochen Sprickerhof ]
  * Move arch dependend Ruby file to cli package

 -- Jochen Sprickerhof <jspricke@debian.org>  Tue, 16 Aug 2022 13:34:01 +0200

ignition-transport (11.0.0+ds-3) unstable; urgency=medium

  * Move to unstable

 -- Jose Luis Rivero <jrivero@osrfoundation.org>  Tue, 11 Jan 2022 18:14:02 +0000

ignition-transport (11.0.0+ds-2) experimental; urgency=medium

  [ Jose Luis Rivero ]
  * Fix patch format
  * Forwarded patch to upstream
  * Update msgs version for -dev package
  * Use an independent package for cli tools
  * Remove transitional package

  [ Jochen Sprickerhof ]
  * Remove wrong dependency
  * Add missing autopkgtest dependency

  [ Jose Luis Rivero ]
  * Depend on fixed version of ignition-utils

 -- Jose Luis Rivero <jrivero@osrfoundation.org>  Wed, 08 Dec 2021 19:23:56 +0000

ignition-transport (11.0.0+ds-1) experimental; urgency=medium

  [ Jose Luis Rivero ]
  * Bump standards to 4.6.0
  * Update metadata in copyright
  * Do not use gitlab custom file
  * Update copyright file
  * Patch and support for relocating binaries in libexec
  * Include cli helper binaries in -dev file
  * Update compat to 13
  * Use hardening=+all option
  * Install ruby files for ign-tools in -dev
  * Add ignition-tools to suggests
  * Update dependency on ignition-utils
  * Add autopkgtest for cmake

  [ Jochen Sprickerhof ]
  * Add patch for broken libzmq3-dev

  [ Jose Luis Rivero ]
  * New upstream version 11.0.0+ds
  * Update metadata to SONAME 11

 -- Jose Luis Rivero <jrivero@osrfoundation.org>  Tue, 23 Nov 2021 23:05:25 +0000

ignition-transport (8.0.0+dfsg-3) unstable; urgency=medium
  * Team upload.
  * Fix autopkgtest and add dependencies

 -- Jochen Sprickerhof <jspricke@debian.org>  Sat, 18 Apr 2020 19:34:20 +0200

ignition-transport (8.0.0+dfsg-2) unstable; urgency=medium

  * Team upload.
  * Update d/watch to Github (switch suffix to ds)
  * Upload to unstable (Closes: #936728, #903286, #897326, #954615, #944925)

 -- Jochen Sprickerhof <jspricke@debian.org>  Sat, 18 Apr 2020 11:35:53 +0200

ignition-transport (8.0.0+dfsg-1) experimental; urgency=medium

  * Team upload.
  * Update d/watch
  * New upstream version 8.0.0+dfsg
  * Rebase and add patch
  * Update copyright
  * Update package names due to ABI change
  * Update packaging

 -- Jochen Sprickerhof <jspricke@debian.org>  Mon, 13 Apr 2020 20:42:46 +0200

ignition-transport (4.0.0+dfsg-4) unstable; urgency=medium

  * Include the missing the yaml file to fix current broken ign-tools support

 -- Jose Luis Rivero <jrivero@osrfoundation.org>  Fri, 09 Mar 2018 17:24:48 +0000

ignition-transport (4.0.0+dfsg-3) unstable; urgency=medium

  * Fix conflict with self -dev package

 -- Jose Luis Rivero <jrivero@osrfoundation.org>  Thu, 08 Mar 2018 23:01:16 +0000

ignition-transport (4.0.0+dfsg-2) unstable; urgency=medium

  * Include the ign-tools ruby module into the packaging
    (Closes: #892378)

 -- Jose Luis Rivero <jrivero@osrfoundation.org>  Thu, 08 Mar 2018 15:47:09 +0000

ignition-transport (4.0.0+dfsg-1) unstable; urgency=medium

  [ Jose Luis Rivero ]
  * New upstream version 4.0.0+dfsg
    (Closes: #883757)
    (Closes: #888017)

  [ Anton Gladky ]
  * Switch to compat-level 11
  * Set Standards-Version: 4.1.3
  * Update VCS-fields (move to salsa)

 -- Jose Luis Rivero <jrivero@osrfoundation.org>  Mon, 05 Feb 2018 14:04:13 +0000

ignition-transport (1.3.0-5) unstable; urgency=medium

  * Fix use system gtest patch
    (Closes: #844878)

 -- Jose Luis Rivero <jrivero@osrfoundation.org>  Mon, 28 Nov 2016 11:44:19 +0000

ignition-transport (1.3.0-4) unstable; urgency=medium

  * Fix pkgconfig file. Ignition transport 1.x series does not use ignition-msgs
   (Closes: #836384)

 -- Jose Luis Rivero <jrivero@osrfoundation.org>  Sat, 10 Sep 2016 14:43:28 +0000

ignition-transport (1.3.0-3) unstable; urgency=medium

  * Added patch for pkgconfig file
   (Closes: #836384)

 -- Jose Luis Rivero <jrivero@osrfoundation.org>  Tue, 06 Sep 2016 21:22:27 +0000

ignition-transport (1.3.0-2) unstable; urgency=medium

  * Fix compilation for protobuf3
    (Closes: #835429)

 -- Jose Luis Rivero <jrivero@osrfoundation.org>  Tue, 30 Aug 2016 23:41:05 +0000

ignition-transport (1.3.0-1) unstable; urgency=medium

  [ Jose Luis Rivero ]
  * Remove old transitional package
  * Include all versions of ignition-transport cmake modules
  * Update standards version
  * Imported Upstream version 1.3.0 (Closes: #789379)
  * Do not use Node for testing but Uuid
  * Imported Upstream version 1.2.0

  [ Anton Gladky ]
  * Drop -dbg package

 -- Jose Luis Rivero <jrivero@osrfoundation.org>  Mon, 25 Jul 2016 16:58:56 +0000

ignition-transport (0.9.0-3) unstable; urgency=medium

  * Miss dependency on protobuf by libignition-transport0-dev
  * Fix URL is Vcs lines

 -- Jose Luis Rivero <jrivero@osrfoundation.org>  Wed, 13 Jan 2016 14:29:00 +0000

ignition-transport (0.9.0-2) unstable; urgency=medium

  * Fix autotest using versioned -dev files

 -- Jose Luis Rivero <jrivero@osrfoundation.org>  Tue, 12 Jan 2016 20:47:23 +0000

ignition-transport (0.9.0-1) unstable; urgency=medium

  * Imported Upstream version 0.9.0
  * Removed patches merged upstream
  * Back to setup the c++11 patch. Needed for building.
  * Fix patch to keep test compilation running
  * Transition to versioned -dev since API follow semantic versioning
  * Remove all -dev install file
  * Implement overrides for transport0
  * Include pkgconfig in dependencies for pkgautotest

 -- Jose Luis Rivero <jrivero@osrfoundation.org>  Tue, 17 Nov 2015 02:00:30 +0100

ignition-transport (0.8.1+dfsg1-1) unstable; urgency=medium

  * ignition-transport 0.8.1-1 release (Closes: #783520)

 -- Jose Luis Rivero <jrivero@osrfoundation.org>  Fri, 19 Jun 2015 15:47:57 +0000
